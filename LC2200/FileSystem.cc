#include "FileSystem.h"
#include "Constants.h"
#include <fstream>
#include <iostream>
#include "Helper.h"
#include <stdio.h>
#include "MyString.h"
using namespace std;

//constructor
FileSystem::FileSystem(){}


//Constructor:
FileSystem::FileSystem(uint numNodes){
  numInodes = numNodes;
  currNode = 1;// the / dir
  numFreeInodes = numInodes - BYTESPERNUM;//-2
  ifstream diskFile("Disk");
  if(diskFile != NULL){
    //ASSERT:disk file does already exist, get numNodes from it and
    //set numInodes to that number you get.
    //diskFile.close();
    DISK = new fstream("Disk",ios::in | ios::out);
    DISKHEX = new fstream("Disk.hex",ios::in | ios::out);
    //Because Disk exists Disk.hex should exist.
    //ASSERT:have opened Disk file in in and out, opened Disk.hex file
    //in in mode.
    
    numInodes = getNumInodes();
    //make NumInodes = NumInodes from previous masterInode
    numFreeInodes = getNumFreeInodes();
  }
  else{
    //ASSERT:Disk file does not exist, create new one and format
    diskFile.close();
    DISK = new fstream("Disk", ios::out);
    DISK->close();
    DISK->open("Disk",ios::in | ios::out);
    DISKHEX = new fstream("Disk.hex",ios::out);
    DISKHEX->close();
    
    //ASSERT:have opened Disk file in in and out, opened Disk.hex file
    //in in mode.,
    //now format Disk and Disk.hex
    format();
  }
  TEMP = new fstream("TEMP",ios::out);
  TEMP->close();
  TEMP->open("TEMP",ios::in | ios::out);
}




//PRE:object satisifs the ci
//POST:returns the currNode
uint FileSystem::getCurrNode(){
  return currNode;
}

//PRE:object is defined and satisfies the CI, mainly numInodes >0
//POST:formats the fileSystem held in the DISK file, resets the
//masterInode and root inode to have no subdirectories and for
//everynode to be free except the masterInode and root directory /.
void FileSystem::format(){
  initialzeDISK();
  //fill DISK file with all 0's for all Inodes
  formatMasterInode();
  //create Master Inode correctly
  formatRootInode();
  //create RootInode correctly
  formatFreeInodes();
  //formats freeInodes correctly.
  currNode = 1;
  //make currNode be root
    
}






//PRE:object is defined and satisfies the CI
//POST:fills the DISK file up with 0's for every Inode
void FileSystem::initialzeDISK(){
  DISK->seekp(0,ios::beg);
  //ASSERT:we are at the beggining of the file
  uint numBytes = INODEBYTES * numInodes;//32*numInodes
  for(int index = 0; index < numBytes; index ++){
    uint formatNum = 0;
    DISK->put((char)formatNum);
  }
  //ASSERT:DISK is initialzed with all 0's
  //ASSERT:DISKHEX is initialzed in hex with all 0's
}

//PRE:object is defined and satisfies the CI
//POST:makes all bytes 0 in the free Inode so it can be made without
//any garbage data
void FileSystem::intialzeFreeInode(uint inodeNum){
  uint byteStart = inodeNum * INODEBYTES;
  DISK->seekp(byteStart,ios::beg);
  
  for(uint index = 0; index <  INODEBYTES; index ++){
    DISK->put((char)0);
  }
}
//PRE:object is defined and satisfies the CI
//POST:formats the MasterInode, correctly putting in how many Inodes
//it has and the address of its first and last freeInode
void FileSystem::formatMasterInode(){
  DISK->seekp(0,ios::beg);
  //DISKHEX->seekp(0,ios::beg);
  //now put in num free Inodes, will be numInodes - 2, for super and
  //root,
  char putNumInodes[BYTESPERNUM];
  
  numToTwoChars(putNumInodes,numInodes);
  changeFreeInodes(numInodes - BYTESPERNUM);//numInodes - 2;
  //ASSERT:we have input the number of freeInodes in Disk and DISKHEX
  DISK->put(putNumInodes[0]);
  DISK->put(putNumInodes[1]);
  //ASSERT:we have put the number of inodes into DISK
  //disk hex stuff
  changeFirstFree(TWO);
  changeLastFree(numInodes - 1);
  //ASSERT:master Inode has been formatted
  
}

//PRE:object is defined and satisfies the CI
//POST:changes the value in the filesystem masterinode that says what
//the address of the first free inode is to firstFree
void FileSystem::changeFirstFree(uint firstFree){
  DISK->seekp(BYTEFIRSTFREE,ios::beg);
  //ASSERT:cursor is at firstFreeByte value in materinode
  char putFirstFree[BYTESPERNUM];
  numToTwoChars(putFirstFree,firstFree);
  DISK->put(putFirstFree[0]);
  DISK->put(putFirstFree[1]);
  //ASSERT:have inserted new value for firstFree into masterInode
}

//PRE:object is defined and satisfies the CI
//POST:changes the value in the filesystem masterInode that says what
//the address of the last free inode is to lastfree
void FileSystem::changeLastFree(uint lastFree){
  DISK->seekp(BYTELASTFREE,ios::beg);
  char putLastFree[TWO];
  numToTwoChars(putLastFree,lastFree);
  DISK->put(putLastFree[0]);
  DISK->put(putLastFree[1]);
  //ASSERT:have inserted new value for lastFree into master Inode
}
//PRE:object is defined and satisfies the CI, cursor in file is at
//masterInode place that where you put in number of freeInodes
//POST:converts the numberof Inodes - 2 to two separate chars, and
//inputs them into DISK and inputs the hex into DISKHEX
void FileSystem::changeFreeInodes(uint number){
  DISK->seekp(0,ios::beg);
  char putNumFreeInodes[BYTESPERNUM];
  numToTwoChars(putNumFreeInodes, number);
  DISK->put(putNumFreeInodes[0]);
  DISK->put(putNumFreeInodes[1]);
  //ASSERT:numFreeInodes in masterInode has been changed to number
}

//PRE:object is defined and satisfies the CI,,array will always be of
//size 2.
//POST:converts the uint to 2 chars, assuming the number is less than
//65,535
void FileSystem::numToTwoChars(char array[],uint number){
  //use masks to get rid of the first byte  of number, then push it
  //over 8 bits to right, then convert that to char and make that 0 in
  //array,(do this only if number is over 255, else array[0]=(char)0,
  //then just use mask on second byte and make that (char)=
  //array[1]. then you done. same kinda thing for getting the number,
  //convert the byte to a uint than move it over to right by 8, then
  //get second byte and place it in the first byte
  uint bitMask = BITMASKSTART;
  uint firstBitMask = bitMask << BITSPERBYTE;//shift left 8
  //now firstBitMask is all 0's except for byte 2 which is FF
  uint secondBitMask = bitMask;
  //second bit mask is all 0's except Byte 3 which is FF
  uint num1 = number & firstBitMask;
  num1 = num1 >> BITSPERBYTE;
  uint num2 = number & secondBitMask;
  array[0] = (char)num1;
  array[1] = (char)num2;
}

//PRE:object is defined and satisfies the CI, Disk exists.
//POST:returns the number of INodes in the fileSystem.
uint FileSystem::getNumInodes(){
  uint rv;
  DISK->seekp(NUMNODESBYTE, ios::beg);
  //ASSERT:cursor is at the start of where we can get total number of
  //nodes
  rv = getNumber();
}

//PRE:object satisfies the CI, cursor in DISK is at place where we
//want to get the number
//POST:returns a number constructed from the 2 bytes that will be
//gotten.
uint FileSystem::getNumber(){
  uint rv = 0;
  char firstChar = DISK->get();
  char secondChar = DISK->get();
  uint firstNum = (uint)firstChar;
  uint secondNum = (uint)secondChar;
  rv = rv | firstNum;
  rv = rv << BITSPERBYTE;//shift left 8.
  rv = rv | secondNum;
  return rv;
}

//PRE:object is defined and satisfies the CI
//POST:formats the root inode correctly, putting in / for the name
//of it.
void FileSystem::formatRootInode(){
  char rootName[TWO];
  rootName[0] = '/';
  rootName[1] = EOS;
  
  putName(rootName,1);
  //ASSERT:root has been named correctly.
  //and formatted correctly?
}

//PRE:object is defined and satisfies the CI
//POST:formats all the freeInodes correctly linking them
void FileSystem::formatFreeInodes(){
  makeFreeInode(TWO,0,TWO + 1);
  
  //ASSERT:we have made the first freeInode
  uint index = TWO + 1;
  while(index < numInodes - 1){
    //ASSERT:we have not reached the last free Inode we must make
    makeFreeInode(index,index - 1,index + 1);
    index ++;
  }
  //ASSERT:have made all middle free inodes
  makeFreeInode(numInodes - 1,numInodes - TWO,0);
  //ASSERT:have created the last Inode, next for that is 0
}

//PRE:object satisfies the CI and object exists
//POST:creates a free inode at inode address InodeNum, makes its
//previous freeinode be equal to previnode and next free inode equal
//to nextInode
void FileSystem::makeFreeInode(uint inodeNum, uint prevInode,uint nextInode){
  uint byteStart = inodeNum * INODEBYTES;
  DISK->seekp(byteStart,ios::beg);
  char array[TWO];
  changeType(inodeNum,FREEINODETYPE);
  //ASSERT:the file type has been put in,
  char charPrevInode[TWO];
  char charNextInode[TWO];
  numToTwoChars(charPrevInode,prevInode);
  numToTwoChars(charNextInode,nextInode);
  //ASSERT:the two arrays hold the bytes of the numbers
  DISK->seekp(byteStart + BYTEPREVINODE,ios::beg);//byteStart + 28
  DISK->put(charPrevInode[0]);
  DISK->put(charPrevInode[1]);
  DISK->put(charNextInode[0]);
  DISK->put(charNextInode[1]);
  //ASSERT:the freeInode has been made correctly
}

//PRE:object is defined and satisfies the CI,name can be held in the
//bytes allocated for names of directories and files,max 20 bytes.
//POST:inputs the name that is held in name into the Inode specified
//by InodeNum
void FileSystem::putName(char name[], uint inodeNum){
  uint nameStart = (inodeNum * INODEBYTES) + BYTENAMESTART;
  DISK->seekp(nameStart,ios::beg);
  uint index = 0;
  while(name[index] != EOS){
    //ASSERT:we have not reached the end of name yet
    DISK->put(name[index]);
    index ++;
  }
  //ASSERT:name of directory or file should be in the correct place
  //now.
  DISK->put((char)0);
}

//PRE:object is defined, InodeNum names an Inode that holds a file, or
//directory inode, also assuming every directory and file has an EOS
//byte
//POST:returns the name of the inode as a MyString object
void FileSystem::getName(uint inodeNum,char name[]){
  uint byteNameStart = (inodeNum * INODEBYTES) + 1;
  uint index = 0;
  DISK->seekp(byteNameStart,ios::beg);
  //ASSERT:cursor is at correct position
  bool stop = false;
  while(!stop){
    char aChar = DISK->get();
    if((uint)aChar == 0){
      //ASSERT:have reached EOS byte
      stop = true;
    }
    else{
      //ASSERT:have not reached EOS Byte
      name[index] = aChar;
      index ++;
    }
   
  }
  name[index] = EOS;
}

//PRE:object is defined and satisfies the CI
//POST:returns the inode address of the next free Inode
uint FileSystem::getNextFreeInode(){
  uint rv;
  DISK->seekp(BYTEFIRSTFREE,ios::beg);
  rv = getNumber();
  return rv;
}

//PRE:object is defined and satisfies the CI
//POST:returns the inode address of the last free Inode
uint FileSystem::getLastFreeInode(){
  uint rv;
  DISK->seekp(BYTELASTFREE,ios::beg);
  rv = getNumber();
  return rv;
}

//PRE:object is defined and satisfies the CI
//POST:takes the input given to the shell, removes the directory if it
//exists, otherwise returns an error
void FileSystem::rmdir(char input[],bool & error, MyString & outString){
  uint parentNode;
  char name[NAMESIZE];
  if(input[0] == SLASH){
    //ASSERT:this is an absolute path, parse input
    MyString dirNames[MAXDIRS];
    uint numDirs;
    getDirName(input,name,dirNames,numDirs,error);
    if(numDirs == 1){
      //ASSERT:we are in root node
      removeDirectory(name,1,error,outString);
    }
    else{
      if(error){
	outString = outString + "This is not a valid name";
      }
      else{
	//ASSERT:name should hold dir name
	checkValidity(name,error);
	if(error){
	  outString = outString + "This is not a valid name";
	}
	else{
	  //ASSERT:name is good
	  parentNode = findInode(input,error);
	  if(error){
	  //ASSERT:this dir doesnt exist
	    outString = outString + "This directory doesnt exist";
	  }
	  else{
	    removeDirectory(name,parentNode,error,outString);
	  }
	}
      }
    }
  }
  else{
    //we are searching for this in our current home node
    parentNode = currNode;
    if(getLength(input) >= NAMESIZE- 1){//-1 cause it stops counting
					//-at EOS, we count EOS
      //ASSERT:The name is too big
      error = true;
      outString = outString + "The name of this directory is too large";
    }
    else{
      //ASSERT:the name size is fine
      checkValidity(input,error);
      if(error){
	//ASSERT:name is not valid
	outString = outString + "This is not a valid name";
      }
      else{
	//ASSERT:name was fine
	removeDirectory(input,parentNode,error,outString);
      }
    }
  }
}

//PRE:object is defined and satisfies the CI
//POST:returns the type of the node
uint FileSystem::getType(uint numNode){
  uint byteStart = (numNode * INODEBYTES) + BYTEDIRTYPE;
  DISK->seekp(byteStart,ios::beg);
  char num = DISK->get();
  uint rv = (uint)num;
  
  return rv;
}

//PRE:object is defined and satisfies the CI
//POST:searches the children under parentNode  and if any match then
//it removes the directory that matches the name and updates inodes
//that are affected
void FileSystem::removeDirectory(char name[],uint parentNode,bool & error,MyString & outString){
  //do leftmost check
  uint currentChild = getChild(parentNode);
  char childName[NAMESIZE];
  getName(currentChild,childName);
  bool areSame = compare(name,childName);
  if(areSame){
    //ASSERT:we found the dir to remove and it is leftmost child
    if(getType(currentChild) == DIRTYPE){
      if(getNumChildren(currentChild) == 0){
	deiterateChildren(parentNode);
	uint nextNode = getNextSibling(currentChild);
	changeLeftMostChild(parentNode,nextNode);//make nextNode leftmost
	changePrevSibling(nextNode,0);
	uint numFreeNodes = getNumFreeInodes();
	numFreeNodes += 1;
	changeFreeInodes(numFreeNodes);
	//ASSERT:number of freeInodes has been updated
	changeType(currentChild,FREETYPE);
	//ASSERT:type of Inode has been changed
	
	if(numFreeNodes == 1){
	  //ASSERT:this is only free node
	  changeFirstFree(currentChild);
	  changeLastFree(currentChild);
	  makeFreeInode(currentChild,0,0);
	}
	else{
	  //ASSERT: it is not only free node
	  uint prevLastFree = getLastFreeInode();
	  changeNextSibling(prevLastFree,currentChild);
	  changePrevSibling(currentChild,prevLastFree);
	  changeLastFree(currentChild);
	  if(currNode == currentChild){
	    //ASSERT:we are deleting the node we are currently in
	    currNode = getParent(currentChild);
	    //since you are deleting this node make the current dir the parent
	  }
	  makeFreeInode(currentChild,prevLastFree,0);
	}
      }
      else{
	error = true;
	outString = outString + "This directory has children";
      }
    }
    else{
      error = true;
      outString = outString + "This is not a directory";
    }
  }
  else{
    //ASSERT:dir to remove is not the leftmost child
    bool found = false;
    uint prevSibling;
    while((!found) && (currentChild != 0)){
      prevSibling = currentChild;
      currentChild = getNextSibling(currentChild);
      char aName[NAMESIZE];
      getName(currentChild,aName);
      found = compare(name,aName);
    }
    if(currentChild == 0){
      //ASSERT:there was no node that matched the name
      error = true;
      outString = outString + "There was no matching directory name";
    }
    else{
      //ASSERT:there was a node that matched the name
      if(getType(currentChild) == DIRTYPE){
	if(getNumChildren(currentChild) == 0){
	  uint prevLastFree = getLastFreeInode();
	  uint nextSibling = getNextSibling(currentChild);
	  changeNextSibling(prevSibling,nextSibling);
	  if(nextSibling != 0){
	    changePrevSibling(nextSibling,prevSibling);
	  }
	  deiterateChildren(parentNode);
	  uint numFreeNodes = getNumFreeInodes();
	  numFreeNodes += 1;
	  changeFreeInodes(numFreeNodes);
	  changeNextSibling(prevLastFree,currentChild);
	  changeLastFree(currentChild);
	  if(currNode == currentChild){
	    //ASSERT:we are deleting the node we are currently in
	    currNode = getParent(currentChild);
	    //since you are deleting this node make the current dir the parent
	  }
	  makeFreeInode(currentChild,prevLastFree,0);
	}
	else{
	  error = true;
	  outString = outString + "This directory has children";
	}
      }
      else{
	error = true;
	outString = outString + "This is not a directory";
      }
    }
  }
}




//PRE:object is defined and satisfies the CI
//POST:returns the number of freeInodes in the master Inode
uint FileSystem::getNumFreeInodes(){
  DISK->seekp(0,ios::beg);
  uint rv = getNumber();
  return rv;
}


//PRE:object is defined and satisfies the CI
//POST:takes the input given to the shell to see if it is an absolute
//path, if it is it calls makeDirectory with the parent node it found
//during the search for the path, if it couldnt find it it returns an
//error, and if it is not an absolute path parentNode = currentNode
void FileSystem::mkdir(char input[],bool & error, MyString & outString){
  uint parentNode;
  bool absolute = false;
  char name[NAMESIZE];//size of 21
  if(input[0] == SLASH){
    //ASSERT:This is an absolute path, parse input
    absolute = true;
    //parse path and name of file into 2 things
    //check length of name and validity
    //find parent node through path and make parentNode = it, make
    //function
    MyString dirNames[MAXDIRS];
    uint numDirs;
    getDirName(input,name,dirNames,numDirs,error);
    if(numDirs == 1){
      //ASSERT:make it in root dir
      makeDirectory(name,1,error,outString);
    }
    else{
      if(error){
	outString = outString + "This is not a valid name";
      }
      else{
      //ASSERT:name should hold dirname
	checkValidity(name,error);
	if(error){
	  //ASSERT:name is bad
	  outString = outString + "This is not a valid name";
	}
	else{
	  //ASSERT:name is good
	  parentNode = findInode(input,error);
	  
	  if(error){
	    //ASSERT:This directory doesnt exist,
	    outString = outString + "This directory doesnt exist";
	  }
	  else{
	    //ASSERT:directory does exist and is held in parentNode
	    makeDirectory(name,parentNode,error,outString);
	  }
	}
      }
    }
  }
  else{
    //ASSERT:this is not an absolute path, check input
    parentNode = currNode;
    if(getLength(input) >= NAMESIZE- 1){//-1 cause it stops counting
					//-at EOS, we count EOS
      //ASSERT:The name is too big
      error = true;
      outString = outString + "The name of this directory is too large";
    }
    else{
      //ASSERT:the name size is fine
      checkValidity(input,error);
      if(error){
	//ASSERT:name is not valid
	outString = outString + "This is not a valid name";
      }
      else{
	//ASSERT:name was fine
	makeDirectory(input,parentNode,error,outString);
      }
    }
  }
}

//PRE:object is defined and satisfies the CI
//POST:returns the Inode address of the iNode being reference by the
//path held in path if it exists, otherwise returns a boolean error by
//reference
uint FileSystem::findInode(char path[], bool & error){
  //ASSERT:path[0] should be slash so start
  uint index = 1;
  uint currentNode = getChild(1); // get child of root(/) node
  uint rv = 1;
  bool found = false;
  while((currentNode != 0) && (!error) && (!found)){
    char name[NAMESIZE];
    char pathName[NAMESIZE];
    uint index1 = 0;
    while((path[index] != SLASH) && (path[index] != EOS)){
      pathName[index1] = path[index];
      index1 ++;
      index ++;
    }
    pathName[index1] = EOS;
    getName(currentNode,name);
    MyString aname = name;
    MyString aPath = path;
    bool same = compare(name,pathName);
    if(same){
      //ASSERT:the names are the same
      //check next index, if it is EOS than this is the return node
      //else current node == child.
      if(path[index] == EOS){
	//ASSERT:this is the parent inode so return this parent
	//current inode as the parent
	found = true;
	rv = currentNode;
      }
      else{
	//ASSERT:we have to get the to the next parent
	index += 1;//so it now starts not on the slash
	currentNode = getChild(currentNode);
      }
    }
    else{
      //ASSERT:The names are not the same
      currentNode = getNextSibling(currentNode);
      
      if(currentNode == 0){
	
	error = true;
	//ASSERT:we couldnt find it
      }
    }
  }
  if(currentNode == 0){
    
    error = true;
    //ASSERT:we couldnt find it
  }
  //ASSERT: rv should hold the inode address of the inode specified by
  //path 
  return rv;
  
}

//PRE:object is defined and satisfies the CI
//POST:returns the leftmost child of the node
uint FileSystem::getChild(uint numInode){
  uint byteStart = (numInode * INODEBYTES) + BYTECHILD;
  DISK->seekp(byteStart,ios::beg);
  uint rv = getNumber();
  return rv;
}


//PRE:object is defined and satisfies the CI
//POST:returns the parent of inode
uint FileSystem::getParent(uint numInode){
  uint byteStart = (numInode * INODEBYTES) + BYTEPARENT;
  DISK->seekp(byteStart,ios::beg);
  uint rv = getNumber();
  return rv;
}

//PRE:object is defined and satisfies the CI
//POST:counts the / in a path
uint FileSystem::countSlashes(char path[]){
  uint rv = 0;
  uint index = 0;
  while(path[index] != EOS){
    if(path[index] == SLASH){
      rv ++;
    }
    index ++;
  }
  return rv;
}

//PRE:object is defined and satisfies the CI
//POST:gets the directory name from the path
void FileSystem::getDirName(char path[], char dirName[],MyString dirNames[],uint & numDirs,bool & error){
  numDirs = countSlashes(path);
  
  uint length = getLength(path);
  uint start = length - 1;
  uint index = 0;
  bool badIndex = false;
  uint putEOS;
  while((path[start] != SLASH) && (!badIndex)){
    index ++;
    if(index >= NAMESIZE){
      //ASSERT:name is too big
      badIndex = true;
    }
    start --;
  }
  //ASSERT:start holds the index of the slash before name of directory
  putEOS = start;
  if(badIndex){
    //ASSERT:bad name
    error = true;
  }
  else{
    //ASSERT:name is fine
    start +=1;//so index isnt at slash
    uint newIndex = 0;
    while(path[start] != EOS){
      dirName[newIndex] = path[start];
      newIndex ++;
      start ++;
    }
    //ASSERT:at end of dirName, put EOS
    dirName[newIndex] = EOS;
    
    path[putEOS] = EOS;
    //putEos here so we can search the path correctly
  }
  
}

//PRE:object is defined and satisfies the CI
//POST:checks if input only has valid characters, returns error as
//true otherwise
void FileSystem::checkValidity(char input[],bool & error){
  uint index = 0;
  while((input[index] != EOS) && (!error)){
    char checkChar = input[index];
    uint check = (uint)checkChar;
    if((check >= VALIDASCIINUMSTART) && (check <= VALIDASCIINUMEND)){
    }//do nothing
    else if((check >=VALIDASCIIUPPERCASESTART) && (check <= VALIDASCIIUPPERCASEEND)){
    }//do nothing
    else if((check >= VALIDASCIILOWERCASESTART) && (check <= VALIDASCIILOWERCASEEND)){
    }//do nothing
    else{
      //ASSERT: this is a bad value
      error = true;
    }
    index ++;
  }
}

//PRE:object is defined and satisfies the CI
//POST:creates a new directory Inode if there is a freeInode, if there
//is not a freeInode it returns an error with a suitable message in
//outString
void FileSystem::makeDirectory(char name[],uint parentNode,bool & error, MyString & outString){
  if(getNumFreeInodes() == 0){
    //There are no free inodes to make a dir with
    error = true;
    outString = outString + "There is no space to make a directory";
  }
  else{
    //There is a free Inode to make a directory with.
    uint freeInode = getNextFreeInode();
    uint currNumFreeInodes = getNumFreeInodes() - 1;
    changeFreeInodes(currNumFreeInodes);
    //ASSERT: -1 to numFreeInodes
    if(currNumFreeInodes == 0){
      //ASSERT:next and last should be 0
      changeFirstFree(0);
      changeLastFree(0);
    }
    else{
      //ASSERT:next should be freeInodes next freeInode
      uint nextFree = getNextSibling(freeInode);
      changeFirstFree(nextFree);
      changePrevSibling(nextFree,0);
      //ASSERT:freeInodes have been done correctly
    }
    //ASSERT:next free inode has been correctly made
    changeType(freeInode,DIRTYPE);
    putName(name,freeInode);
    
    putParent(freeInode,parentNode);
    changeNumChildren(freeInode,0);
    changeLeftMostChild(freeInode,0);
    //ASSERT:everything has been premade about the directory except
    //for its next sibling and previous sibling, which will be done in
    //placedirectory

   
    placeDirectory(freeInode,parentNode);
    //ASSERT:directory has been placed and all nodes have been updated
    iterateChildren(parentNode);//+1 to children of parentnode
    //ASSERT:we have placed the dir so add one to its parents children
    
  }
}

//PRE:object is defined and satisfies the CI
//POST:figures out where to place the directory depending on the
//parents current children, will place the directories in alphabetical
//order
void FileSystem::placeDirectory(uint dirNode, uint parentNode){
  if(getNumChildren(parentNode) == 0){
    //ASSERT:the parent has no children so just place it and make it
    //the leftmost
    changeLeftMostChild(parentNode,dirNode);
    changeNextSibling(dirNode,0);//no next
    changePrevSibling(dirNode,0);//no prev
    
  }
  else{
    //ASSERT:parentNode has children
    uint currentChild = getChild(parentNode);
    bool lessThan = isLessThan(dirNode,currentChild);
    //do leftMostCheck
    if(lessThan){
      //ASSERT:this will be new leftmost child of parent
      changeLeftMostChild(parentNode,dirNode);
      changePrevSibling(currentChild,dirNode);
      changeNextSibling(dirNode,currentChild);
      changePrevSibling(dirNode,0);
    }
    else{
      //ASSERT:will not b leftmost child of parent
      uint prevSibling;
      while((!lessThan) && (currentChild != 0)){
	prevSibling = currentChild;
	currentChild = getNextSibling(currentChild);
	lessThan = isLessThan(dirNode,currentChild);
      }
      //ASSERT:we have found the place to place dirNode
      if(currentChild == 0){
	//do nothing
      }
      else{
	//assert the next sibling exists
	changePrevSibling(currentChild,dirNode);
      }
      changeNextSibling(dirNode,currentChild);
      changePrevSibling(dirNode,prevSibling);
      changeNextSibling(prevSibling,dirNode);
      //ASSERT:node has been placed correctly and all nodes
      //surrounding have been updated
    }
  }
}

//PRE:object is defined and satisfies the CI
//POST:returns true if name of node1 < name of node2, false otherwise
bool FileSystem::isLessThan(uint node1,uint node2){
  bool rv = false;
  char name1[NAMESIZE];
  char name2[NAMESIZE];
  getName(node1,name1);
  getName(node2,name2);
  MyString Name1 = name1;
  MyString Name2 = name2;
  if(Name1 > Name2){
    rv = true;
  }
  return rv;
}


//PRE:object is defined and satisfies the CI,assumes DISK is pointing
//to right position
//PRE:inputs the number specified wherever cursor is at the time
void FileSystem::putNumber(uint number){
  char array[TWO];
  numToTwoChars(array,number);
  DISK->put(array[0]);
  DISK->put(array[1]);
  //ASSERT:number has been inputted
}

//PRE:object is defined and satisfies the CI
//POST:changes the leftmost child of the node
void FileSystem::changeLeftMostChild(uint inodeNum,uint childNum){
  uint byteStart = (inodeNum * INODEBYTES) + BYTECHILD;
  DISK->seekp(byteStart,ios::beg);
  putNumber(childNum);
}

//PRE:object is defined and satisfies the CI
//POST:changes the type of the inode depending on the uint specified.
void FileSystem::changeType(uint inodeNum, uint type){
  uint byteStart = inodeNum * INODEBYTES;
  DISK->seekp(byteStart,ios::beg);
  DISK->put((char)type);
}

//PRE:object is defined and satisfies the CI
//POST:sets the number of children of the node to be num
void FileSystem::changeNumChildren(uint inodeNum,uint numChildren){
  uint byteStart = (inodeNum * INODEBYTES) + BYTENUMCHILDREN;
  DISK->seekp(byteStart,ios::beg);
  putNumber(numChildren);
}

//PRE:object is defined and satisfies the CI
//POST:changes the parent of the inode to the parent specified
void FileSystem::putParent(uint inodeNum, uint parent){
  uint byteStart = (inodeNum * INODEBYTES) + BYTEPARENT;
  DISK->seekp(byteStart,ios::beg);
  putNumber(parent);
}

//PRE:object is defined and satisfies the CI
//POST:changes the number of children of the inode by +1
void FileSystem::iterateChildren(uint inodeNum){
  uint byteStart = (inodeNum * INODEBYTES) + BYTENUMCHILDREN;
  DISK->seekp(byteStart,ios::beg);
  uint numChildren = getNumber();
  DISK->seekp(byteStart,ios::beg);
  numChildren += 1;
  putNumber(numChildren);
}


//PRE:object is defined and satisfies the CI
//POST:changes the number of children of the inode by -1
void FileSystem::deiterateChildren(uint inodeNum){
  uint byteStart = (inodeNum * INODEBYTES) + BYTENUMCHILDREN;
  DISK->seekp(byteStart,ios::beg);
  uint numChildren = getNumber();
  DISK->seekp(byteStart,ios::beg);
  numChildren = numChildren - 1;
  putNumber(numChildren);
}

//PREobject is defined and satisfies the CI:
//POST:returns the number of children this inode has
uint FileSystem::getNumChildren(uint inodeNum){
  uint byteStart = (inodeNum * INODEBYTES) + BYTENUMCHILDREN;
  DISK->seekp(byteStart,ios::beg);
  uint rv = getNumber();
  return rv;
}

//PRE:object is defined and satisfies the CI
//POST:changes the previous sibling of a node to the inode address
//specified
void FileSystem::changePrevSibling(uint inodeNum,uint prevSibling){
  uint byteStart = (inodeNum * INODEBYTES) + BYTEPREVINODE;
  DISK->seekp(byteStart,ios::beg);
  putNumber(prevSibling);
}

//PRE:object is defined and satisfies the CI
//POST:changes the next sibling of a node to the inode address
//specified
void FileSystem::changeNextSibling(uint inodeNum,uint nextSibling){
  uint byteStart = (inodeNum * INODEBYTES) + BYTENEXTINODE;
  DISK->seekp(byteStart,ios::beg);
  char array[TWO];
  numToTwoChars(array,nextSibling);
  //ASSERT:prevSibling is held in array as two chars
  DISK->put(array[0]);
  DISK->put(array[1]);
}

//PRE:object is defined and satisfies the CI
//POST:returns the Inodes previous sibling
uint FileSystem::getPrevSibling(uint inodeNum){
  uint byteStart = (inodeNum * INODEBYTES) + BYTEPREVINODE;
  DISK->seekp(byteStart,ios::beg);
  uint rv = getNumber();
  return rv;
}

//PRE:object is defined and satisfies the CI
//POST:returns the Inodes next sibling
uint FileSystem::getNextSibling(uint inodeNum){
  uint byteStart = (inodeNum * INODEBYTES) + BYTENEXTINODE;
  DISK->seekp(byteStart,ios::beg);
  uint rv = getNumber();
  return rv;
}

//PRE:object is defiend and satisfies the CI
//POST:returns the contents of the current directory if path boolean
//is false, returns the contents of the path specified in pathName if
//the path boolean is true, if the directory specified doesnt exist it
//returns an error
void FileSystem::ls(bool path,char pathName[],bool & error, bool & output, MyString & outString){
  if(countSlashes(pathName) == 1){
    output = true;
    lsHelper(1,outString);
    outString.addChar(EOS);
  }
  else{
    if(path){
      //ASSERT:we are looking to find an absolute path
      uint wantNode = findInode(pathName,error);
      if(error){
	//ASSERT:the directory must not exist
	outString = outString + "The directory specified doesnt exist";
      }
      else{
	//ASSERT:there were no problems find the node
	output = true;
	if(getType(wantNode) == DIRTYPE){
	  lsHelper(wantNode,outString);
	  //ASSERT:outString should hold info on children for path
	}
	else{
	  //ASSERT:This is not a dir
	  error = true;
	  outString = outString + "This is not a directory";
	}
      }
    }
    else{
      //ASSERT:return the contents of the current working directory
      output = true;
      lsHelper(currNode,outString);
      //ASSERT:outString should hold info
    }
    outString.addChar(EOS);
  }
}

//PRE:object is defined and satisfies the CI
//POST:puts the names of the children of specified node into outString
void FileSystem::lsHelper(uint nodeNum,MyString & outString){
  uint currChild = getChild(nodeNum);
  uint index = 0;
  while(currChild != 0){
    char name[NAMESIZE];
    getName(currChild, name);
    //ASSERT:name holds name of directory
    outString = outString + name;
    outString.addChar(' ');
    currChild = getNextSibling(currChild);
    index ++;
    if(index == FOUR){
      index = 0;
      outString.addChar('\n');
    }
  }
}


//PRE:object is defined and satisfies the CI
//POST:depending on what is specified in path, the current node will
//renaviagte to a specific node depending on what is specified
void FileSystem::cd(bool empty,char path[], bool & error, bool & output, MyString & outString){
  if(empty){
    //ASSERT:root is currnode now
    currNode = 1;
  }
  else{
    //ASSERT:path was not empty
    if(compare(path,GOTOPARENT)){
      //ASSERT:make current node the parent node
      if(currNode == 1){
	error = true;
	outString = outString + "You cannot go to the parent of root";
      }
      else{
	currNode = getParent(currNode);
      }
    }
    else if(path[0] == SLASH){
      //ASSERT:we are dealing with absolute path
      uint newNode = findInode(path,error);
      currNode = newNode;
      if(error){
	//ASSERT:path doesnt exist
	outString = outString + "This directory path does not exist or is incorrect";
      }
    }
    else{
      //ASSERT:search for name in current directory
      uint currChild = getChild(currNode);
      bool found = false;
      while((!found) && (currChild != 0)){
	char name[NAMESIZE];
	getName(currChild,name);
	if(compare(name,path)){
	  //ASSERT:names are the same, this is the new currNode
	  found = true;
	  if(getType(currChild) == DIRTYPE){
	    currNode = currChild;
	  }
	  else{
	    error = true;
	    outString = outString + "This is not a directory";
	  }
	}
	else{
	  currChild = getNextSibling(currChild);
	}
      }
      if(!found){
	//ASSERT:we never found the dir
	error = true;
	outString = outString + "There is no directory with that name";
      }
    }
  }
}

//PRE:object is defined and satisfies the CI
//POST:returns the absolute path to the current working directory, or
//currNode by way of outString which is passed by reference
void FileSystem::pwd(MyString & outString){
  MyString newString;
  outString = newString;
  //to clear outString
  pwdHelper(currNode,outString);
}

//PRE:object is defined and satisfies the CI
//POST:recursively calls itself until it reaches the master node which
//is the parent of the root node, in this way it adds all the path
//names up correctly
void FileSystem::pwdHelper(uint aNode, MyString & outString){
  if(aNode == 0){
    //ASSERT:we are at the master inode so do nothing
  }
  else{
    //ASSERT: we are not at the master inode
    char name[NAMESIZE];
    getName(aNode,name);
    //ASSERT:name of aNode is held in name
    MyString nameString = name;
    if(aNode != 1){
      nameString.addChar(SLASH);
    }
    outString = nameString + outString;
    //ASSERT:outString correctly holds this part of the path
    uint nextPathNode = getParent(aNode);
    pwdHelper(nextPathNode,outString);
  }
}

//PRE:object satisfies the CI, the startNode and endNode have already
//been parsed and checked for validity by machine
//POST:displays the Inodes in hexadecimal format from Inode startNum
//to Inode endNum
void FileSystem::displayInodes(uint startNode, uint endNode,MyString & outString){
  for(uint index = startNode; index <= endNode; index ++){
    giveOutStringInode(index,outString);
    outString.addChar('\n');
  }
  outString.addChar(EOS);
  //ASSERT:all inodes specified are held in outString in hexidecimal
  //format
}

//PRE:object satisfies the CI,
//POST:gives the outString the information of this inode in hex format
void FileSystem::giveOutStringInode(uint nodeNum,MyString & outString){
  char giveInodeNum[MAXINPUT];
  sprintf(giveInodeNum,"Inode No. %i\n",nodeNum);
  outString = outString + giveInodeNum;
  uint byteStart = nodeNum * INODEBYTES;
  DISK->seekp(byteStart,ios::beg);
  uint counter = 0;
  for(uint index = 0; index < WORDSPERNODE; index ++){
    if(counter == FOUR){
      //ASSERT:put newline
      counter = 0;
      outString.addChar('\n');
    }
    uint word = getWord();
    char addToString[MAXINPUT];
    sprintf(addToString,"0x%08x ",word);
    outString = outString + addToString;
    outString.addChar(' ');
    counter ++;
  }
  //ASSERT:hex of inode has been added to outString
}

//PRE:object satisfies the CI, assumes the cursor in disk is in the
//correct position
//POST:creates a word by getting 4 bytes and then returns the word
//that was created
uint FileSystem::getWord(){
  uint returnWord = 0;
  uint shift = SHIFT1;//24, shift 24 first time, then minus 8, ect
  for(uint index = 0; index < FOUR; index ++){
    uchar word = DISK->get();
    uint putWord = (uint)word <<shift;
    returnWord = returnWord | putWord;
    shift = shift - SHIFTMINUS;
  }
  //ASSERT:the 4 bytes we got are held in returnword in the correct
  //order
  //cerr<<hex<<returnWord<<endl;
  return returnWord;
}

//PRE:object is defined and satisfies the CI, also linux file is an
//open file in ios in and out
//POST:copies the chars from linuxFile into the copyData array
void FileSystem::getData(fstream * linuxFile, uchar copyData[]){
  linuxFile->seekp(0,ios::beg);
  uchar aChar = linuxFile->get();
  uint index = 0;
  while(!linuxFile->eof()){
    //ASSERT:have not reached end of file
    copyData[index] = aChar;
    aChar = linuxFile->get();
    index ++;
  }
  copyData[index] = EOS;
}


//PRE:object satisfies the CI and is defined 
//POST:copies a file from the linux to our directory
void FileSystem::cpPlus(char pathToLinuxFile[],char pathToFile[],bool & error, MyString & outString){
  ifstream aFile(pathToLinuxFile);
  if(aFile != NULL){
    //ASSERT:file exists
    aFile.close();
    fstream * linuxFile = new fstream(pathToLinuxFile,ios::in | ios::out);
    //ASSERT:we have opened the linux file
    linuxFile->seekp(0,ios::end);
    uint numBytes = linuxFile->tellg();
    uint numFileNodes = 1;
    uint numContentNodes =  numBytes / NUMCONTENTBYTES;
    uint leftOverContentBytes = numBytes % NUMCONTENTBYTES;
    if(leftOverContentBytes != 0){
      //ASSERT:There was a leftover byte
      numContentNodes = numContentNodes + 1;
    }
    uint numNeededNodes = numFileNodes + numContentNodes;
    uint inodeNum = findInode(pathToFile,error);
    if(error){
      //ASSERT:this inode doesnt exists so we must make it
      error = false;
      //maybe reuse make directory to make a make file thing,
      //possible reserve content nodes
       uint numFreeInodes = getNumFreeInodes();
       if(numNeededNodes > numFreeInodes){
	 //ASSERT:we dont have space for this file
	 error = true;
	 outString = outString + "Not enough space for file";
       }
       else{
	 uint numCharsNeeded = numBytes;
	 uchar copyData[numBytes];
	 getData(linuxFile,copyData);
	 //ASSERT:data is held in copyDat
	 makeFile(pathToFile,copyData,numBytes,error,outString);
       } 
    }
    else{
      //This inode exists, we must overwrite it, maybe remove it all
      //first so we can check if we have enough content nodes.
      if(getType(inodeNum == FILETYPE)){
	//ASSERT:this is a file so we can remove it
	
	rmFile(pathToFile,error,outString);
	if(numNeededNodes > numFreeInodes){
	  //ASSERT:we dont have space for this file
	  error = true;
	  outString = outString + "Not enough space for file";
	}
	else{
	  uint numCharsNeeded = numBytes;
	  uchar copyData[numCharsNeeded];
	  getData(linuxFile,copyData);
	  //ASSERT:data is held in copyData
	  makeFile(pathToFile,copyData,numBytes,error,outString);
	}
      }
      else{
	//ASSERT:this is not the name of a file
	error = true;
	outString = outString + "This is not the name of a file";
      }
    }
    linuxFile->close();
    delete linuxFile;
  }
  else{
    //ASSERT:file does not exist
    error = true;
    outString = outString + "The linux file specified does not exist";
  
  }
}

//PRE:object satisfies the CI and is defined
//POST:copies a file from our fileSystem to a file in our linux system
void FileSystem::cpMinus(char pathToFile[], char pathToLinuxFile[],bool & error, MyString & outString){
  uint inodeNum;
  if(pathToFile[0] == SLASH){
    inodeNum = findInode(pathToFile,error);
  }
  else{
    inodeNum = findInodeInCurrDir(pathToFile,error);
  }
  if(error){
    //ASSERT:file does not exist
    outString = outString + "This is file does not exist";
  }
  else{
    //ASSERT:file does exist and we have the inode number
    if(getType(inodeNum) == FILETYPE){
      ofstream * linuxFile = new ofstream(pathToLinuxFile);
      //if file exists or not it doesnt matter open new ofstream
      copyToLinuxFile(linuxFile,inodeNum);
      //ASSERT:all data has been copied from inodeNum to linuxFile
      linuxFile->close();
      delete linuxFile;
    }
    else{
      error = true;
      outString = outString + "Incorrect File Type";
    }
  }
}

//PRE:object satisfies the CI
//POST:returns the size of the file in bytes
uint FileSystem::getNumBytes(uint node){
  uint byteStart = (node * INODEBYTES) + BYTESIZESTART;
  DISK->seekp(byteStart,ios::beg);
  uint rv = getNumber();
  return rv;
}

//PRE:object is defined and satisfies the CI, assumes inodeNum is of
//type FILETYPE
//POST:copies the info from the inode number to linux file
void FileSystem::copyToLinuxFile(ofstream * linuxFile, uint inodeNum){
  uint numBytes = getNumBytes(inodeNum);
  //+1 for EOS just incase
  uchar copyData[numBytes];
  getChars(inodeNum,copyData);
  //ASSERT:copyData holds the data from fileNode inodeNum 
  for(uint index = 0; index < numBytes; index ++){
    uchar inChar = copyData[index];
    linuxFile->put(inChar);
  }
  //ASSERT:we have put all data from copyData into linux file
}


//PRE:object satisfies the CI and is defined
//POST:copies a file1 from our filesystem to another file2 in our filesystem
void FileSystem::cpReg(char pathToFile1[],char pathToFile2[], bool & error,MyString & outString){
  if(compare(pathToFile1,pathToFile2)){
    //ASSERT:they are the same input, this is bad
    error = true;
    outString = outString + "You cannot use the same 2 files";
  }
  else{
    //ASSERT:did not put in the same file names
    uint sourceInode;
    if(pathToFile1[0] == SLASH){
      sourceInode = findInode(pathToFile1,error);
    }
    else{
      sourceInode = findInodeInCurrDir(pathToFile1,error);
    }
    if(error){
      //ASSERT:could not find sourceInode
      outString = outString + "The source file does not exist";
    }
    else{
      //ASSERT:we have found the source file
      if(getType(sourceInode) == FILETYPE){
	//ASSERT:the source file exists and is held in sourceInode
	uint pathInode = findInode(pathToFile2,error);
	if(error){
	  //ASSERT:file does not exist, no need to overwrite
	  error = false;
	}
	else{
	  //ASSERT:file does exist, delete it
	  rmFile(pathToFile2,error,outString);
	}
	uint numFreeInodes = getNumFreeInodes();
	uint numBytes = getNumBytes(sourceInode);
	uint numContentNodes = numBytes / CONTENTBYTES;
	if(numBytes % CONTENTBYTES != 0){
	  numContentNodes = numContentNodes + 1;
	}
	if(numFreeInodes < numContentNodes){
	  error = true;
	  outString = outString + "There are not enough free Inodes";
	}
	else{
	  //ASSERT:we have enough free Inodes
	  uint numCharsNeeded = numContentNodes * NUMCONTENTBYTES + 1;//??*25
	  //+1 for EOS just incase
	  uchar copyData[numCharsNeeded];
	  getChars(sourceInode,copyData);
	  makeFile(pathToFile2,copyData,numBytes,error,outString);
	}
      }
      else{
	error = true;
	outString = outString + "This is not a file";
      }
    }
  }
}

//PRE:object is defined and satisfies the CI
//POST:creates the file with data from the char array
void FileSystem::makeFile(char pathToFile[],uchar copyData[],uint numBytes,bool & error,MyString & outString){

  uint thisIndex = 0;
  char name[NAMESIZE];
  uint parentNode = 1;
  bool absolute = false;
  if(pathToFile[0] == SLASH){
    absolute = true;
    //we are dealing with an absolute path
    MyString dirNames[MAXDIRS];
    uint numDirs;
    getDirName(pathToFile,name,dirNames,numDirs,error);
    //name holds name of file
    if(error){
      //ASSERT:name is too long
    }
    else{
      parentNode = findInode(pathToFile,error);
      if(error){
	//ASSERT:cant find parentnode
	error = true;
	outString = outString + "invalid path";
      }
    }
  }
  else{
    //ASSERT:this is not a path
    parentNode = currNode;
    uint index = 0;
    if(getLength(pathToFile) > NAMESIZE - 1){
      //Error name length
      error = true;
      outString = outString + "invalid name size";
    }
    
  }
  if(!error){
    
    //ASSERT:there is no error
    //There is a free Inode to make a directory with.
    uint freeInode = getNextFreeInode();
    changeType(freeInode,FILETYPE);
    uint currNumFreeInodes = getNumFreeInodes() - 1;
    changeFreeInodes(currNumFreeInodes);
    //ASSERT: -1 to numFreeInodes
    if(currNumFreeInodes == 0){
      //ASSERT:next and last should be 0
      changeFirstFree(0);
      changeLastFree(0);
    }
    else{
      //ASSERT:next should be freeInodes next freeInode
      uint nextFree = getNextSibling(freeInode);
      changeFirstFree(nextFree);
      changePrevSibling(nextFree,0);
      //ASSERT:freeInodes have been done correctly
    }
    //ASSERT:next free inode has been correctly made
    if(absolute){
      putName(name,freeInode);  
    }
    else{
      putName(pathToFile,freeInode);
    }
    putParent(freeInode,parentNode);
    changeLeftMostChild(freeInode,0);
    //ASSERT:everything has been premade about the directory except
    //for its next sibling and previous sibling, which will be done in
    //placedirectory
    putNumBytes(freeInode,numBytes);
    
    
    placeDirectory(freeInode,parentNode);
    //ASSERT:directory has been placed and all nodes have been updated
    iterateChildren(parentNode);//+1 to children of parentnode
    //ASSERT:we have placed the dir so add one to its parents
    
    putContent(copyData,freeInode);
    //creates the content nodes for the fileNode and puts content in
    //them
  }
}

//PRE:object is defined and satisfies CI
//POST:puts the number of bytes for file into inodeNum
void FileSystem::putNumBytes(uint inodeNum,uint numBytes){
  uint byteStart = (inodeNum * INODEBYTES) + BYTESIZESTART;
  DISK->seekp(byteStart,ios::beg);
  putNumber(numBytes);
}

//PRE:object is defined and satisfies the CI, there is a freeInode to get
//POST:returns the next free inode and updates the masterInode to
//reflect that the next free inode is no longer there
uint FileSystem::returnAndUpdateNextFreeInode(){
  uint freeInode = getNextFreeInode();
  uint currNumFreeInodes = getNumFreeInodes() - 1;
  changeFreeInodes(currNumFreeInodes);
  //ASSERT: -1 to numFreeInodes
  if(currNumFreeInodes == 0){
    //ASSERT:next and last should be 0
    changeFirstFree(0);
    changeLastFree(0);
  }
    else{
      //ASSERT:next should be freeInodes next freeInode
      uint nextFree = getNextSibling(freeInode);
      changeFirstFree(nextFree);
      changePrevSibling(nextFree,0);
      //ASSERT:freeInodes have been done correctly
    }
  //ASSERT:master Inode has been updated correctly
  return freeInode;
}

//PRE:object is defined and satisfies the CI, we have enough
//freeInodes to put all the content in
//POST:puts the content held in the copyData char array into content
//inodes
void FileSystem::putContent(uchar copyData[],uint fileNode){
  uint contentCounter = 0;
  uint prevContent = 0;
  uint numBytes = getNumBytes(fileNode);
  uint currContent = returnAndUpdateNextFreeInode();
  changeLeftMostChild(fileNode,currContent);
  changePrevSibling(currContent,0);
  changeType(currContent,CONTENTTYPE);
  putParent(currContent,fileNode);
  uint byteStart = (currContent * INODEBYTES) + CONTENTSTART;
  DISK->seekp(byteStart,ios::beg);

  for(uint index = 0; index < numBytes; index ++){
    //ASSERT:we havent encountered EOS char
    uchar inChar = copyData[index];
    DISK->put(inChar);
    contentCounter ++;
    if(contentCounter > NUMCONTENTBYTES - 1){
      //ASSSERT:we ran out of space in inode
      contentCounter = 0;
      prevContent = currContent;
      currContent = returnAndUpdateNextFreeInode();
      //parent and master inode updated
      changeNextSibling(prevContent,currContent);
      changePrevSibling(currContent,prevContent);
      putParent(currContent,fileNode);
      changeType(currContent,CONTENTTYPE);
      //siblings and other miscallenous stuff about content nodes updated
      byteStart = (currContent * INODEBYTES) + CONTENTSTART;
      DISK->seekp(byteStart,ios::beg);
      //reset bytestart and disk cursor
    }
  }
  changeNextSibling(currContent,0);
  
}

//PRE:object is defined and satisfies the CI
//POST:copies the content from the path from filesystem into the char
//array 
void FileSystem::getChars(uint sourceInode, uchar copyData[]){
  uint copyIndex = 0;
  uint contentIndex = 0;
  uint currContentNode = getChild(sourceInode);
 
  uint numBytes = getNumBytes(sourceInode);
  
  uint byteStart = (currContentNode * INODEBYTES) + CONTENTSTART;
  DISK->seekp(byteStart,ios::beg);
  uchar inChar = DISK->get();
  for(uint index = 0; index < numBytes; index ++){
    //ASSERT:The character is not the EOS char
    copyData[copyIndex] = inChar;
    copyIndex ++;
    contentIndex ++;
    if(contentIndex > NUMCONTENTBYTES - 1){
      //ASSERT:move to next content Byte
      currContentNode = getNextSibling(currContentNode);
      byteStart = (currContentNode * INODEBYTES) + CONTENTSTART;
      DISK->seekp(byteStart,ios::beg);
    }
    inChar = DISK->get();
  }
}


//PRE:object is defined and satisfies the CI
//POST:copies the content from the ofstream object in our linux
//filesystem into the char array 
void FileSystem::getChars(fstream * linuxFile, uchar copyData[]){
  linuxFile->seekp(0,ios::beg);
  uint index = 0;
  while(!linuxFile->eof()){
    char aChar = linuxFile->get();
    copyData[index] = aChar;
  }
}



//PRE:object is defined and satisfies the CI
//POST:removes the file if it exists and is a file, otherwise returns
//an error
void FileSystem::rmFile(char pathToFile[],bool & error,MyString & outString){
  uint parentNode;
  char name[NAMESIZE];
  if(pathToFile[0] == SLASH){
    //ASSERT:this is an absolute path, parse input
    MyString dirNames[MAXDIRS];
    uint numDirs;
    getDirName(pathToFile,name,dirNames,numDirs,error);
    //can still be used for file as going through directories to get
    //their names.
    if(numDirs == 1){
      //ASSERT:we are in root node
      uint len = getLength(name);
      if(name[len - 1] == STAR){
	name[len - 1] = EOS;
	removeSimilarFiles(name,1);
	//ASSERT:remove all names matching name
      }
      else{
	//ASSERT:just remove name matching this
	removeFile(name,1,error,outString);
      }
    }
    else{
      if(error){//error will come from getDirName
	outString = outString + "This is not a valid name, there is a problem with the file names";
      }
      else{
	//ASSERT:name should hold file name
	//ASSERT:name is good
	parentNode = findInode(pathToFile,error);
	if(error){
	  //ASSERT:this dir doesnt exist
	  outString = outString + "This file doesnt exist";
	}
	else{
	  //ASSERT:This dir does exist 
	  uint len = getLength(name);
	  if(name[len - 1] == STAR){
	    name[len - 1] = EOS;
	    removeSimilarFiles(name,1);
	    //ASSERT:remove all names matching name
	  }
	  else{
	    //ASSERT:just remove this name
	    removeFile(name,parentNode,error,outString);
	  }
	}
      }
    }
  }
  else{
    //we are searching for this in our current home node
    parentNode = currNode;
    if(getLength(pathToFile) >= NAMESIZE- 1){//-1 cause it stops counting
					//-at EOS, we count EOS
      //ASSERT:The name is too big
      error = true;
      outString = outString + "The name of this file is too large";
    }
    else{
      //ASSERT:the name size is fine
      //ASSERT:name was fine
      uint len = getLength(pathToFile);
      if(pathToFile[len - 1] == STAR){
	pathToFile[len - 1] = EOS;
	removeSimilarFiles(pathToFile,1);
	//ASSERT:remove all names matching name
      }
      else{
	//ASSERT:just remove this name
	removeFile(pathToFile,parentNode,error,outString);
      }
    }
  }
}

//PRE:object is defined and satisfies the CI
//POST:removes all files that match up to the end of partName
void FileSystem::removeSimilarFiles(char partName[], uint parent){
  uint currChild = getChild(parent);
  while(currChild != 0){
    uint nextChild = getNextSibling(currChild);
    if(getType(currChild) == FILETYPE){
      //ASSERT:This is a file so check it
      char name[NAMESIZE];
      getName(currChild,name);
      //ASSERT:childs name is is name
      if(areSimilar(partName,name)){
	//ASSERT:names are similar
	removeFile(currChild,parent);
      }
    }
    currChild = nextChild;
  }
}

//PRE:object is defined and satisfies the CI, assumes the node
//specified is a file  
//POST:removes the file and updates the parent and siblings
//accordingly
void FileSystem::removeFile(uint currChild,uint parent){
  uint leftMost = getChild(parent);
  if(currChild = leftMost){
    //ASSERT:This is the leftmost child
    uint firstContent = getChild(currChild);
    deiterateChildren(parent);
    uint nextChild = getNextSibling(currChild);
    changeLeftMostChild(parent,nextChild);
    uint numFree = getNumFreeInodes() + 1;
    changeFreeInodes(numFree);
    if(numFree == 1){
      changeFirstFree(currChild);
      changeLastFree(currChild);
      makeFreeInode(currChild,0,0);
    }
    else{
      uint lastFree = getLastFreeInode();
      changeNextSibling(lastFree,currChild);
      changeLastFree(currChild);
      makeFreeInode(currChild,lastFree,0);
    }
    removeContent(firstContent);
  }
  else{
    //ASSERT:This is not the leftMost child
    uint firstContent = getChild(currChild);
    deiterateChildren(parent);
    uint numFree = getNumFreeInodes() + 1;
    changeFreeInodes(numFree);
    if(numFree == 1){
      changeFirstFree(currChild);
      changeLastFree(currChild);
      makeFreeInode(currChild,0,0);
    }
    else{
      uint lastFree = getLastFreeInode();
      changeNextSibling(lastFree,currChild);
      changeLastFree(currChild);
      makeFreeInode(currChild,lastFree,0);
    }
    removeContent(firstContent);
  }
}

//PRE:object is defined and CI is satisfied
//POST:returns treu if all of partName is in name, false otherwise
bool FileSystem::areSimilar(char partName[],char name[]){
  uint index = 0;
  bool rv = true;
  while((partName[index] != EOS) && (rv)){
    if(partName[index] != name[index]){
      //ASSERT:names are not similar
      rv =false;
    }
    index ++;
  }
  return rv;
}


//PRE:object is defined and satisfies the CI
//POST:removes the file specified, searches the parent node specified
//for the pathname that is specified, used if we do not know that the
//path actually exists or not
void FileSystem::removeFile(char name[],uint parentNode, bool & error,MyString & outString){
   //do leftmost check
  uint currentChild = getChild(parentNode);
  char childName[NAMESIZE];
  getName(currentChild,childName);
  bool areSame = compare(name,childName);
  if(areSame){
    //ASSERT:we found the dir to remove and it is leftmost child
    if(getType(currentChild) == FILETYPE){
      uint firstContent = getChild(currentChild);
      deiterateChildren(parentNode);
      uint nextNode = getNextSibling(currentChild);
      changeLeftMostChild(parentNode,nextNode);//make nextNode leftmost
      changePrevSibling(nextNode,0);
      uint numFreeNodes = getNumFreeInodes();
      numFreeNodes = numFreeNodes + 1;
      changeFreeInodes(numFreeNodes);
      //ASSERT:number of freeInodes has been updated
      changeType(currentChild,FREETYPE);
      //ASSERT:type of Inode has been changed
      
      if(numFreeNodes == 1){
	//ASSERT:this is only free node
	changeFirstFree(currentChild);
	changeLastFree(currentChild);
	makeFreeInode(currentChild,0,0);
      }
      else{
	//ASSERT: it is not only free node
	uint prevLastFree = getLastFreeInode();
	changeNextSibling(prevLastFree,currentChild);
	changePrevSibling(currentChild,prevLastFree);
	changeLastFree(currentChild);
	makeFreeInode(currentChild,prevLastFree,0);
      }
      removeContent(firstContent);
    }
    else{
      error = true;
      outString = outString + "This is not a File";
    }
  }
  else{
    //ASSERT:file to remove is not the leftmost child
    bool found = false;
    uint prevSibling;
    while((!found) && (currentChild != 0)){
      prevSibling = currentChild;
      currentChild = getNextSibling(currentChild);
      char aName[NAMESIZE];
      getName(currentChild,aName);
      found = compare(name,aName);
    }
    if(currentChild == 0){
      //ASSERT:there was no node that matched the name
      error = true;
      outString = outString + "There was no matching file name";
    }
    else{
      //ASSERT:there was a node that matched the name
      if(getType(currentChild) == FILETYPE){
	uint firstContent = getChild(currentChild);
	uint prevLastFree = getLastFreeInode();
	uint nextSibling = getNextSibling(currentChild);
	changeNextSibling(prevSibling,nextSibling);
	if(nextSibling != 0){
	  changePrevSibling(nextSibling,prevSibling);
	}
	deiterateChildren(parentNode);
	uint numFreeNodes = getNumFreeInodes();
	numFreeNodes = numFreeNodes + 1;
	changeFreeInodes(numFreeNodes);
	changeNextSibling(prevLastFree,currentChild);
	changeLastFree(currentChild);
	makeFreeInode(currentChild,prevLastFree,0);
	removeContent(firstContent);
      }
      else{
	error = true;
	outString = outString + "This is not a File";
      }
    }
  }
}

//PRE:object is defined and paramter is a content node
//number of freeInodes cant be zero as we delete the fileInode before
//any content inodes
//POST:recursively deletes all of the content nodes that are attached
//to a fileNode
void FileSystem::removeContent(uint contentNode){
  if(contentNode == 0){
    //Base case do nothing
  }
  else{
    uint numFreeNodes = getNumFreeInodes() + 1;
    changeFreeInodes(numFreeNodes);
    
    uint lastFreeInode = getLastFreeInode();
    uint nextContent = getNextSibling(contentNode);
    changeType(contentNode,FREETYPE);
    makeFreeInode(contentNode,lastFreeInode,0);
    removeContent(nextContent);
    
  }
}


//PRE:object is defined and satisfies the CI
//POST:gives the information from the fileNode specified to outString,
//unless the fileNode does not exist or is not a fileNode
void FileSystem::cat(char path[],bool h,bool & error,bool & output,MyString & outString){
  if(path[0] == SLASH){
    //ASSERT:we are looking at absolute path
    uint inodeNum = findInode(path,error);
    if(error){
      outString = outString + "This file does not exist";
    }
    else{
      //ASSERT:we found the inode
      if(getType(inodeNum) == FILETYPE){
	//ASSERT:this is a file inode
	catHelper(inodeNum,h,outString);
	output = true;
      }
      else{
	//ASSERT:This is not a file inod
	error = true;
	outString = outString + "This is not a file";
      }
    }
  }
  else{
    //ASSERT:This is not an absolute path, look in currNode
    uint inodeNum = findInodeInCurrDir(path,error);
    if(error){
      //ASSERT:did not find it
      outString = outString + "This file does not exist";
    }
    else{
      //ASSERT: found it
      if(getType(inodeNum) == FILETYPE){
	catHelper(inodeNum,h,outString);
	output = true;
      }
      else{
	error = true;
	outString = outString + "This is not a file";
      }
    }
    
  }
}

//PRE:object satisfies the CI
//POST:gives information about the inode specified to the outString
void FileSystem::catHelper(uint inodeNum, bool h, MyString & outString){
  
  uint numBytes = getNumBytes(inodeNum);
  uchar copyData[numBytes];
  
  getChars(inodeNum,copyData);
  //ASSERT:chars are held in copyData
  if(h){
    //ASSERT:h flag was specified
    for(uint index = 0; index < numBytes; index ++){
      uchar aChar = copyData[index];
      uint putChar = (uint)aChar;
      char putIn[BASE];
      sprintf(putIn,"%x",putChar);
      outString = outString + putIn;
    }
    outString.addChar(EOS);
  }
  else{
    //ASSERT:h flag was not specified
    outString = outString + copyData;
    outString.addChar(EOS);
  }
}


//PRE:object is defined and satisfies the CI
//POST:returns the inode number if there is an inode that matches in
//your current dir, otherwise throws error
uint FileSystem::findInodeInCurrDir(char name[],bool & error){
  uint currChild = getChild(currNode);
  bool found = false;
  uint rv = 0;
  while((currChild != 0) && (!found)){
    char aName[NAMESIZE];
    getName(currChild,aName);
    if(compare(name,aName)){
      //ASSERT:names are the same
      found = true;
      rv = currChild;
    }
    currChild = getNextSibling(currChild);
  }
  if(!found){
    //ASSERT:never found it
    error = true;
  }
  return rv;
}



//PRE:object is defined and satisfies the CI
//POST:gives certain information to the outString
void FileSystem::du(char path[],bool & error,bool & output,MyString & outString){
  if(path[0] == SLASH){
    //ASSERT:absolute path
    uint inodeNum = findInode(path,error);
    if(error){
      outString = outString + "There is no dir with this path";
    }
    else{
      //Found inode
      if(getType(inodeNum) == DIRTYPE){
	//ASSERT: is dir
	uint numBytes = duHelper(inodeNum,outString) * INODEBYTES;
	output = true;
	char array[MAXINPUT];
	sprintf(array,"Number of Bytes: %i",numBytes);
	outString = outString + array;
      }
      else{
	error = true;
	outString = outString + "This is not a directory";
      }
    }
  }
  else{
    //not absolute path
    uint inodeNum = findInodeInCurrDir(path,error);
    if(error){
      outString = outString + "There is no dir with this name";
    }
    else{
      //ASSERT:found node
      if(getType(inodeNum) == DIRTYPE){
	//ASSERT: is dir
	uint numBytes = duHelper(inodeNum,outString) * INODEBYTES;
	output = true;
	char array[MAXINPUT];
	sprintf(array,"Number of Bytes: %i",numBytes);
	outString = outString + array;
      }
      else{
	error = true;
	outString = outString + "This is not a directory";
      }
    }
  }
}


//PRE:object is defined and satisfies the CI, assumes inodenum is a
//directory
//POST:gives information about the directory to the outstring
uint FileSystem::duHelper(uint inodeNum,MyString & outString){
  uint rv = 1;//cause it counts this dir as 1
  uint child = getChild(inodeNum);
  while(child != 0){
    if(getType(child) == DIRTYPE){
      //ASSERT;we have a dir
      rv = rv + duHelper(getChild(child),outString);
      child = getNextSibling(child);
    }
    else{
      //ASSERT:must be a file
      uint fileSize = getNumChildren(child);
      uint numNodes = (fileSize / NUMCONTENTBYTES) + 1;
      if(fileSize % NUMCONTENTBYTES > 0){
	numNodes = numNodes + 1;
      }
      rv = rv + numNodes;
      child = getNextSibling(child);
    }
  }
  return rv;
}

//PRE:object is defined and satisfies the CI
//POST:copies the inode to temporary storage
void FileSystem::copyToTemp(uint inodeNum){
  TEMP->seekp(0,ios::beg);
  uint byteStart = inodeNum * INODEBYTES;
  DISK->seekp(byteStart);
  for(int index = 0; index < INODEBYTES; index ++){
    char aChar = DISK->get();
    TEMP->put(aChar);
  }
  //ASSERT:entire inodeNum has been copied to TEMP
}


//PRE:object is defined and satisfies the CI
//POST:copies whatever is in temp to inodeNum
void FileSystem::copyFromTemp(uint inodeNum){
  TEMP->seekp(0,ios::beg);
  uint byteStart = inodeNum * INODEBYTES;
  DISK->seekp(byteStart);
  for(int index = 0; index < INODEBYTES; index ++){
    char aChar = TEMP->get();
    DISK->put(aChar);
  }
  //ASSERT:inode has been taken out of temp and put in inodeNum place
}

//PRE:object is defined and satisfies the CI
//POST:defragments the fileSystem
void FileSystem::defrag(){
  int numFreeNodes = getNumFreeInodes();
  int numNodes = getNumInodes();
  int lastInode = numNodes - 1;
  int startInode = FILETYPE;//2,0=master,1=root
  while((lastInode > startInode) && (numFreeNodes != 0)){
    int usedInode;
    bool foundLast = false;
   
    while((!foundLast) && (lastInode > startInode)){
      if(getType(lastInode) != FREETYPE){
	//ASSSERT:the type of inode is not free
	foundLast = true;
	usedInode = lastInode;
	copyToTemp(usedInode);

	//ASSERT:this inode has been copied to temp and are keeping track of it in usedInode
      }
      else{
	//ASSERT:it is a freeInode
	lastInode --;
	numFreeNodes --;//deiterate because we are passing over a freeNode
      }
    }
    bool foundStart = false;
    uint firstFree;
    while((!foundStart) && (lastInode > startInode)){
      if(getType(startInode) == FREETYPE){
	foundStart = true;
	firstFree = startInode;
	numFreeNodes --;//cause we swapping the nodes
      }
      else{
	startInode ++;
      }
    }
    //ASSERT:usedInode is in temp and firstFree is a freeInode
    uint prevFree = getPrevSibling(firstFree);
    uint nextFree = getNextSibling(firstFree);
    uint parent = getParent(usedInode);
    uint prev = getPrevSibling(usedInode);
    uint next = getNextSibling(usedInode);
    if(prevFree == 0){
      //ASSERT:This was first free in master
      changeFirstFree(usedInode);
    }
    else{
      changeNextSibling(prevFree,usedInode);
    }
    if(nextFree == 0){
      //ASSET:this was last free in master
      changeLastFree(usedInode);
    }
    else{
      changePrevSibling(nextFree,usedInode);
    }
    //ASSERT:freeInode stuff has been updated, now just make free inode
    if(prev == 0){
      //ASSERT:this is the parents leftmost
      changeLeftMostChild(parent,firstFree);
    }
    else{
      changeNextSibling(prev,firstFree);
    }
    if(next != 0){
      //ASSERT:there is a next sibling
      changePrevSibling(next,firstFree);
    }
    //ASSERT:stuff around this node has been updated so just copy from temp
    makeFreeInode(usedInode,prevFree,nextFree);
    //we have made usedInode into a free inode
    copyFromTemp(firstFree);
    //usedInode has been copied from temp to firstFree
    //Everything should be correct at this point
  }
  //Defrag is done
  
}

//Destructor:
FileSystem::~FileSystem(){
  DISKHEX->open("Disk.hex",ios::in | ios::out);
  DISK->seekp(0,ios::beg);
  DISKHEX->seekp(0,ios::beg);
  TEMP->close();
  delete TEMP;
  uint newLineIndex = 0;
  while(!DISK->eof()){
    //ASSERT:we have not reached the end of file
    if(newLineIndex == WORDSPERNODE){
      DISKHEX->put('\n');
      newLineIndex = 0;
    }
    uint aWord = getWord();
    char array[BITS];
    sprintf(array,"%08x",aWord);
    uint index = 0;
    while(array[index] != EOS){
      if(index % TWO == 0){
	DISKHEX->put('|');
      }
      DISKHEX->put(array[index]);
      index ++;
      
    }
    DISKHEX->put('\n');
    newLineIndex ++;
    //ASSERT:inputted char as hex 
  }
  DISK->close();
  DISKHEX->close();
  delete DISK;
  delete DISKHEX;
}

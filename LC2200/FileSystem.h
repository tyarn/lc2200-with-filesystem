#ifndef INCLUDED_FILESYSTEM
#define INCLUDED_FILESYSTEM
#include "Constants.h"
#include <fstream>
#include <iostream>
#include "MyString.h"
using namespace std;

//CI:fileSystem file called Disk must exist after constructor,
//numInodes > 0

class FileSystem{
 private:
  uint currNode;//represent the current directory node
  fstream * DISK;//Represents the disk file that the filesystem is
		 //held in
  fstream * DISKHEX;//where you write the hex representation of the
		    //disk file after you close thd disk file
  fstream * TEMP;//where you are able to store one inode when defrag
		 //cmd runs
  uint numInodes;//represents the number of inodes
  uint numFreeInodes;//represents the number of freeInodes
 public:
  //constructor
  FileSystem();

  //constructor
  FileSystem(uint numNodes);

  //PRE:object satisifs the ci
  //POST:returns the currNode
  uint getCurrNode();

  
  //PRE:object is defined and satisfies the CI, mainly numInodes >0
  //POST:formats the fileSystem held in the DISK file, resets the
  //masterInode and root inode to have no subdirectories and for
  //everynode to be free except the masterInode and root directory /.
  void format();


  //PRE:object is defined and satisfies the CI
  //POST:fills the DISK file up with 0's for every Inode
  void initialzeDISK();

  //PRE:object is defined and satisfies the CI
  //POST:formats the MasterInode, correctly putting in how many Inodes
  //it has and the address of its first and last freeInode
  void formatMasterInode();

  
  //PRE:object is defined and satisfies the CI
  //POST:changes the value in the filesystem masterinode that says what
  //the address of the first free inode is to firstFree
  void changeFirstFree(uint firstFree);
  
  //PRE:object is defined and satisfies the CI
  //POST:changes the value in the filesystem masterInode that says what
  //the address of the last free inode is to lastfree
  void changeLastFree(uint lastFree);
  

  //PRE:object is defined and satisfies the CI, Disk exists.
  //POST:returns the number of INodes in the fileSystem.
  uint getNumInodes();
  
  
  //PRE:object is defined and satisfies the CI, cursor in file is at
  //masterInode place that where you put in number of freeInodes
  //POST:converts the numberof Inodes - 2 to two separate chars, and
  //inputs them into DISK and inputs the hex into DISKHEX
  void changeFreeInodes(uint number);
  
  //PRE:object is defined and satisfies the CI,
  //POST:converts the uint to 2 chars, assuming the number is less than
  //65,535
  void numToTwoChars(char array[],uint number);
  
  //PRE:object is defined and satisfies the CI
  //POST:formats the root inode correctly, putting in / for the name
  //of it.
  void formatRootInode();

  //PRE:object is defined and satisfies the CI
  //POST:formats all the freeInodes correctly linking them
  void formatFreeInodes();

  //PRE:object satisfies the CI and object exists
  //POST:creates a free inode at inode address InodeNum, makes its
  //previous freeinode be equal to previnode and next free inode equal
  //to nextInode
  void makeFreeInode(uint inodeNum, uint prevInode,uint nextInode);

  //PRE:object is defined and satisfies the CI,name can be held in the
  //bytes allocated for names of directories and files.
  //POST:inputs the name that is held in name into the Inode specified
  //by InodeNum
  void putName(char name[], uint inodeNum);



  //PRE:object is defined, InodeNum names an Inode that holds a file, or
  //directory inode
  //POST:returns the name of the inode as a MyString object
  void getName(uint inodeNum,char name[]);

  
  //PRE:object is defined and satisfies the CI
  //POST:takes the input given to the shell to see if it is an absolute
  //path, if it is it calls makeDirectory with the parent node it found
  //during the search for the path, if it couldnt find it it returns an
  //error, and if it is not an absolute path parentNode = currentNode
  void mkdir(char input[],bool & error, MyString & outString);
  
  //PRE:object is defined and satisfies the CI
  //POST:creates a new directory Inode if there is a freeInode, if there
  //is not a freeInode it returns an error with a suitable message in
  //outString
  void makeDirectory(char name[],uint parentNode,bool & error, MyString & outString);



  
  //PRE:object satisfies the CI
  //POST:returns the size of the file in bytes
  uint getNumBytes(uint node);
  
  //PRE:object is defined and satisfies the CI
  //POST:changes the previous sibling of a node to the inode address
  //specified
  void changePrevSibling(uint inodeNum,uint prevSibling);
  
  //PRE:object is defined and satisfies the CI
  //POST:changes the next sibling of a node to the inode address
  //specified
  void changeNextSibling(uint inodeNum,uint nextSibling);
  
  
  //PRE:object is defined and satisfies the CI
  //POST:returns the inode address of the next free Inode
  uint getNextFreeInode();
  
  //PRE:object is defined and satisfies the CI
  //POST:returns the inode address of the last free Inode
  uint getLastFreeInode();

  
  //PRE:object is defined and satisfies the CI
  //POST:returns the number of free Inodes
  uint getNumFreeInodes();

  
  //PRE:object is defined and satisfies the CI
  //POST:returns the Inodes previous sibling
  uint getPrevSibling(uint inodeNum);
  
  //PRE:object is defined and satisfies the CI
  //POST:returns the Inodes next sibling
  uint getNextSibling(uint inodeNum);


  
  //PRE:object is defined and satisfies the CI
  //POST:checks if input only has valid characters, returns error as
  //true otherwise
  void checkValidity(char input[],bool & error);


  //PRE:object is defined and satisfies the CI
  //POST:gets the directory name from the path
  void getDirName(char path[], char dirName[],MyString dirNames[],uint & numDirs,bool & error);
 


  //PRE:object is defined and satisfies the CI
  //POST:counts the / in a path
  uint countSlashes(char path[]);

  
  //PRE:object is defined and satisfies the CI
  //POST:returns the Inode address of the iNode being reference by the
  //path held in path if it exists, otherwise returns a boolean error by
  //reference
  uint findInode(char path[], bool & error);



  //PRE:object is defined and satisfies the CI
  //POST:returns the type of the node
  uint getType(uint numNode);
  
  //PRE:object is defined and satisfies the CI
  //POST:returns the leftmost child of the node
  uint getChild(uint numInode);

  //PRE:object is defined and satisfies the CI
  //POST:returns the parent of inode
  uint getParent(uint numInode);

  
  //PRE:object is defined and satisfies the CI
  //POST:changes the type of the inode depending on the uint specified.
  void changeType(uint inodeNum, uint type);

  //PRE:object is defined and satisfies the CI
  //POST:changes the parent of the inode to the parent specified
  void putParent(uint inodeNum, uint parent);

  //PRE:object is defined and satisfies the CI
  //POST:changes the number of children of the inode by +1
  void iterateChildren(uint inodeNum);


  //PRE:object is defined and satisfies the CI
  //POST:changes the number of children of the inode by -1
  void deiterateChildren(uint inodeNum);

  //PREobject is defined and satisfies the CI:
  //POST:returns the number of children this inode has
  uint getNumChildren(uint inodeNum);


  //PRE:object is defined and satisfies the CI
  //POST:sets the number of children of the node to be num
  void changeNumChildren(uint inodeNum,uint numChildren);


  
  //PRE:object is defined and satisfies the CI,assumes DISK is pointing
  //to right position
  //PRE:inputs the number specified wherever cursor is at the time
  void putNumber(uint number);
  
  //PRE:object is defined and satisfies the CI
  //POST:changes the leftmost child of the node
  void changeLeftMostChild(uint inodeNum,uint childNum);

  
  //PRE:object is defined and satisfies the CI
  //POST:figures out where to place the directory depending on the
  //parents current children, will place the directories in alphabetical
  //order
  void placeDirectory(uint dirNode, uint parentNode);

  
  //PRE:object is defined and satisfies the CI
  //POST:returns true if name of node1 < name of node2, false otherwise
  bool isLessThan(uint node1,uint node2);

  
  //PRE:object is defined and satisfies the CI
  //POST:takes the input given to the shell, removes the directory if it
  //exists, otherwise returns an error
  void rmdir(char input[],bool & error, MyString & outString);
  

  //PRE:object is defined and satisfies the CI
  //POST:searches the children under parentNode  and if any match then
  //it removes the directory that matches the name and updates inodes
  //that are affected
  void removeDirectory(char name[],uint parentNode,bool & error,MyString & outString);

  
  //PRE:object is defiend and satisfies the CI
  //POST:returns the contents of the current directory if path boolean
  //is false, returns the contents of the path specified in pathName if
  //the path boolean is true, if the directory specified doesnt exist it
  //returns an error
  void ls(bool path,char pathName[],bool & error, bool & output, MyString & outString);


  
  //PRE:object is defined and satisfies the CI
  //POST:puts the names of the children of specified node into outString
  void lsHelper(uint nodeNum,MyString & outString);

  
  //PRE:object is defined and satisfies the CI
  //POST:depending on what is specified in path, the current node will
  //renaviagte to a specific node depending on what is specified
  void cd(bool empty,char path[], bool & error, bool & output, MyString & outString);

  
  //PRE:object is defined and satisfies the CI
  //POST:displays the absolute path to the current working directory, or
  //currNode
  void pwd(MyString & outString);

  
  //PRE:object is defined and satisfies the CI
  //POST:recursively calls itself until it reaches the master node which
  //is the parent of the root node, in this way it adds all the path
  //names up correctly
  void pwdHelper(uint aNode, MyString & outString);
  
  
  //PRE:object satisfies the CI
  //POST:displays the Inodes in hexadecimal format from Inode startNum
  //to Inode endNum
  void displayInodes(uint startNode,uint endNode,MyString & outString);

  
  //PRE:object satisfies the CI,
  //POST:gives the outString the information of this inode in hex format
  void giveOutStringInode(uint nodeNum,MyString & outString);

  
  //PRE:object satisfies the CI, assumes the cursor in disk is in the
  //correct position
  //POST:creates a word by getting 4 bytes and then returns the word
  //that was created
  uint getWord();

  //PRE:object satisfies the CI, cursor in DISK is at place where we
  //want to get the number
  //POST:returns a number constructed from the 2 bytes that will be
  //gotten.
  uint getNumber();

  
  //PRE:object satisfies the CI and is defined 
  //POST:copies a file from the linux to our directory
  void cpPlus(char pathToLinuxFile[],char pathToFile[],bool & error, MyString & outString);
  
  //PRE:object satisfies the CI and is defined
  //POST:copies a file from our fileSystem to a file in our linux system
  void cpMinus(char pathToFile[], char pathToLinuxFile[],bool & error, MyString & outString);
  
  //PRE:object satisfies the CI and is defined
  //POST:copes a file from our filesystem to another file in our filesystem
  void cpReg(char pathToFile1[],char pathToFile2[], bool & error,MyString & outString);


  //PRE:object is defined and satisfies the CI
  //POST:removes the file if it exists and is a file, otherwise returns
  //an error
  void rmFile(char pathToFile[],bool & error,MyString & outString);

  
  //PRE:object is defined and satisfies the CI
  //POST:removes the file specified, searches the parent node specified
  //for the pathname that is specified, used if we do not know that the
  //path actually exists or not
  void removeFile(char name[],uint parentNode, bool & error,MyString & outString);

  
  //PRE:object is defined and paramter is a content node
  //POST:recursively deletes all of the content nodes that are attached
  //to a fileNode
  void removeContent(uint contentNode);

  
  //PRE:object is defined and satisfies the CI
  //POST:copies the content from the path from filesystem into the char
  //array 
  void getChars(uint sourceInode, uchar copyData[]);
  

  //PRE:object is defined and satisfies the CI
  //POST:copies the content from the ofstream object in our linux
  //filesystem into the char array 
  void getChars(fstream * linuxFile, uchar copyData[]);

  
  //PRE:object is defined and satisfies the CI
  //POST:makes all bytes 0 in the free Inode so it can be made without
  //any garbage data
  void intialzeFreeInode(uint inodeNum);


  
  //PRE:object is defined and satisfies the CI
  //POST:creates the file with data from the char array
  void makeFile(char pathToFile[],uchar copyData[],uint numBytes,bool & error,MyString & outString);


  //PRE:object is defined and satisfies the CI
  //POST:puts the content held in the copyData char array into content
  //inodes
  void putContent(uchar copyData[],uint fileNode);

  
  //PRE:object is defined and satisfies CI
  //POST:puts the number of bytes for file into inodeNum
  void putNumBytes(uint inodeNum,uint numBytes);
  
  //PRE:object is defined and satisfies the CI
  //POST:returns the next free inode and updates the masterInode to
  //reflect that the next free inode is no longer there
  uint returnAndUpdateNextFreeInode();

  
  //PRE:object is defined and satisfies the CI, assumes inodeNum is of
  //type FILETYPE
  //POST:copies the info from the inode number to linux file
  void copyToLinuxFile(ofstream * linuxFile, uint inodeNum);

  
  //PRE:object is defined and satisfies the CI, also linux file is an
  //open file in ios in and out
  //POST:copies the chars from linuxFile into the copyData array
  void getData(fstream * linuxFile, uchar copyData[]);

  //PRE:object is defined and satisfies the CI
  //POST:gives the information from the fileNode specified to outString,
  //unless the fileNode does not exist or is not a fileNode
  void cat(char path[],bool h,bool & error,bool & output,MyString & outString);


  //PRE:object is defined and satisfies the CI
  //POST:returns the inode number if there is an inode that matches in
  //your current dir, otherwise throws error
  uint findInodeInCurrDir(char name[],bool & error);


  
  //PRE:object satisfies the CI
  //POST:gives information about the inode specified to the outString
  void catHelper(uint inodeNum, bool h, MyString & outString);

  
  
  //PRE:object is defined and satisfies the CI
//POST:gives certain information to the outString
  void du(char path[],bool & error,bool & output,MyString & outString);

  //PRE:object is defined and satisfies the CI, assumes inodenum is a
  //directory
  //POST:gives information about the directory to the outstring
  uint duHelper(uint inodeNum,MyString & outString);

  //PRE:object is defined and satisfies the CI
  //POST:copies the inode to temporary storage
  void copyToTemp(uint inodeNum);
  
  
  //PRE:object is defined and satisfies the CI
  //POST:copies whatever is in temp to inodeNum
  void copyFromTemp(uint inodeNum);


  
  //PRE:object is defined and satisfies the CI
  //POST:defragments the fileSystem
  void defrag();

  
  //PRE:object is defined and CI is satisfied
  //POST:returns treu if all of partName is in name, false otherwise
  bool areSimilar(char partName[],char name[]);

  
  //PRE:object is defined and satisfies the CI
  //POST:removes all files that match up to the end of partName
  void removeSimilarFiles(char partName[], uint parent);


  
  //PRE:object is defined and satisfies the CI, assumes the node
  //specified is a file  
  //POST:removes the file and updates the parent and siblings
  //accordingly
  void removeFile(uint currChild,uint parent);
  
  ~FileSystem();
  
};
#endif
